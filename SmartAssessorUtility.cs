﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace SmartAssessorShared
{
    public class SmartAssessor
    {
        const string smartAssessorUri = "https://api.smartassessor.co.uk:883/";
        const string API_VERSION = "APIVersion";
        const string apiVersion = "1_0";
        const string CLIENT_KEY = "ClientKey";
        const string clientKey = "8375F5D5-AB45-4351-BBBC-DC8E2D2980A7";
        const string username = "api.smartassessor@liv-coll.ac.uk";
        const string password = "f6330C17-CD2E-4DC4-8D3D-4DFD0BB766E6";
        const string AUTHORIZATION = "Authorization";
        static String authorization = "Bearer ";

        static SmartAssessor()
        {
            try
            {
                using (var handler = new WebRequestHandler())
                {
                    var postData = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("username", username),
                        new KeyValuePair<string, string>("password", password)
                    };

                    using (var content = new FormUrlEncodedContent(postData))
                    {
                        using (var httpClient = new HttpClient(handler))
                        {
                            using (var response = httpClient.PostAsync(smartAssessorUri + "Token", content).Result)
                            {
                                dynamic responseContent = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                                authorization = authorization + Convert.ToString(responseContent.access_token);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static string GetRequest(string url)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
                    webClient.Headers.Add(API_VERSION, apiVersion);
                    webClient.Headers.Add(CLIENT_KEY, clientKey);
                    webClient.Headers.Add(AUTHORIZATION, authorization);
                    return webClient.DownloadString(smartAssessorUri + url);
                }
            }
            catch (Exception e)
            {
                FailComponent(e.ToString());
                return null;
            }
        }

        public static string PostRequest(string url, string json)
        {
            try
            {
                using (var webClient = new WebClient())
                {

                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    webClient.Headers.Add(API_VERSION, apiVersion);
                    webClient.Headers.Add(CLIENT_KEY, clientKey);
                    webClient.Headers.Add(AUTHORIZATION, authorization);
                    return webClient.UploadString(smartAssessorUri + url, json);
                }
            }
            catch (Exception e)
            {
                FailComponent(e.ToString());
                return null;
            }
        }

        private static void FailComponent(string errorMsg)
        {
            //SOME KIND OF LOGGING
        }
    }
}
